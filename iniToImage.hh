/*
 * iniToImage.h
 *
 *  Created on: Aug 5, 2014
 *      Author: Nathan Steurs
 */

#ifndef INITOIMAGE_H_
#define INITOIMAGE_H_

#include "EasyImage.h"
#include "ini_configuration.hh"
#include "MyL2D.h"
#include "MyL3D.hh"
#include "vector.hh"
#include "Figure.hh"
#include "DrawHelpers.hh"
#include "FiguresHelpers.hh"
#include "Zbuffer.hh"
#include "Face.hh"

namespace ini2image {
class iniToImage {
public:
	iniToImage();
	img::EasyImage processIniL2D(const ini::Configuration& conf);
	img::EasyImage processIniL3D(const ini::Configuration& conf);
	img::EasyImage processIniWireframe(const ini::Configuration& conf);
	virtual ~iniToImage();
};
}
#endif /* INITOIMAGE_H_ */
