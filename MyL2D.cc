/*
 * MyL2D.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: Nathan Steurs
 */

#include "MyL2D.h"
#include <cmath>

#include <algorithm>
#include <iostream>

MyL2D::MyL2D(std::istream &in)  : LParser::LSystem2D(in){
	xMax = -200000; //HUGE_VALF throws exceptions
	yMax = -200000;
	xMin = 200000;
	yMin = 200000;
	my_x = 0;
	my_y = 0;
}

MyL2D::~MyL2D() {
	// TODO Auto-generated destructor stub
}

void MyL2D::AddLine(double a, double b, double c, double d, double e, double f){
	line l(a,b,c,d,e,f);
	this->lines.push_back(l);
	if(a < xMin){
		xMin = a;
	}
	if(b < xMin ){
		xMin = b;
	}
	if(a > xMax){
		xMax = a;
	}
	if(b > xMax){
		xMax = b;
	}

	if(c < yMin){
		yMin = c;
	}
	if(d < yMin ){
		yMin = d;
	}
	if(c > yMax){
		yMax = c;
	}
	if(d > yMax){
		yMax = d;
	}
}

bool MyL2D::Exists(char& c){
	for(const char& k : alphabet){
		if((c == k)&&(drawfunction[c] == true)){
			return true;
		}
	}
	return false;
}

void MyL2D::PprintLines(std::vector<line> p){
	for(auto temp : p){
		std::cout << "["<< temp.getX1() << "," << temp.getY1() << "]";
		std::cout << "["<< temp.getX2() << "," << temp.getY2() << "]" << std::endl;
	}
}

img::EasyImage MyL2D::ScaleSystem(int size, img::Color& c, img::Color& bgcolor){
	//TODO change to use Drawhelpers
	double xRange = (xMax - xMin);
	if(xRange == 0) xRange = 0.5;
	//TODO Clean this hack up
	//std::cout << "xRange:" << xRange << std::endl;
	double yRange = (yMax - yMin);
	//TODO Clean this hack up
	if (yRange == 0) yRange = 0.5;
	//std::cout << "yRange:" << yRange << std::endl;
	//std::cout << "xmax xmin ymax ymin" << xMax << xMin << yMax << yMin << std::endl;
	double ImageX = size*(xRange/(std::max(xRange, yRange)));//Used to have round
	double ImageY = size*(yRange/(std::max(xRange, yRange)));//Used to have round
	double d = 0.95*(ImageX/xRange);
	double DcX = ((d*(xMin + xMax))/2.0);
	double DcY = ((d*(yMin + yMax))/2.0);
	double dx = ((ImageX/2.0) - DcX);
	double dy = ((ImageY/2.0) - DcY);
	img::EasyImage img(ImageX, ImageY, bgcolor);
	//std::cout << "Creating image of size:" << ImageX << "|" << ImageY << std::endl;
	std::vector<line> scaledlines;
	for (auto oldline : lines){
		double newx1 = round((oldline.getX1()*d) + dx);
		double newy1 = round((oldline.getY1()*d) + dy);
		double newx2 = round((oldline.getX2()*d) + dx);
		double newy2 = round((oldline.getY2()*d) + dy);
		line l(newx1, newx2, newy1, newy2);
		scaledlines.push_back(l);
	}
	for(auto t : scaledlines){
		img.draw_line(t.getX1(), t.getY1(), t.getX2(), t.getY2(), c);
	}
	return img;
}
img::EasyImage MyL2D::DrawSystem(){
	img::EasyImage img;
	return img;
}

void MyL2D::Iterate(std::string rule, int iterations){
	//TODO Make the whole iteration 1 method
	for(auto c : rule){
		if(c == '+'){
			startingAngle += angle;
		}
		else if(c == '-'){
			startingAngle -= angle;
		}
		else if((c == '(')||(c == '[')||(c == '{')){
			Position pos(my_x, my_y, startingAngle);
			Brackets.push(pos);
		}
		else if((c == ')')||(c == ']')||(c == '}')){
			my_x = Brackets.top().getX();
			my_y = Brackets.top().getY();
			startingAngle = Brackets.top().getAngle();
			Brackets.pop();
		}
		else if(Exists(c)){
			if(iterations == 1){
			//std::cout << "Drawing a simple line for L2D for char:" << c << std::endl;
			double x2 = my_x + std::cos(DegToRad(startingAngle));
			double y2 = my_y + std::sin(DegToRad(startingAngle));
			//std::cout << "Adding line with values:" << my_x << x2 << my_y << y2 << std::endl;
			AddLine(my_x, x2, my_y, y2, 0, 0);
			my_x = x2; //update position
			my_y = y2;
			}
			else if(iterations > 1){
				Iterate(get_replacement(c), (iterations - 1));
			}
		}
		else{
			//Don't draw anything because it's a symbol we either don't use, or has a Drawfunction of 0
		}
	}
}

double MyL2D::DegToRad(double angle){
	return ((M_PI/180)*angle);
}

void MyL2D::Create(){
	for(auto c : initiator) {
		if(c == '+'){
			startingAngle += angle;
		}
		else if(c == '-'){
			startingAngle -= angle;
		}
		else if((c == '(')||(c == '[')||(c == '{')){
					Position pos(my_x, my_y, startingAngle);
					Brackets.push(pos);
				}
				else if((c == ')')||(c == ']')||(c == '}')){
					my_x = Brackets.top().getX();
					my_y = Brackets.top().getY();
					startingAngle = Brackets.top().getAngle();
					Brackets.pop();
				}
		else if(Exists(c)){
			auto str = get_replacement(c);
			if(nrIterations > 0){
				Iterate(str, nrIterations);
			}
			else if(nrIterations == 0){
				//std::cout << "Drawing with 0 iterations, should print the original string." << std::endl;
				Iterate(initiator,1);
				//Just draw the string without replacing anything
			}
			else{
				Iterate(str, 1);//Was 1;
			}

		}
	}
}

// POSITION CLASS FOR BRACKETS

Position::Position(double x1, double y1, double newangle){
	x = x1;
	y = y1;
	angle = newangle;
}

Position::~Position(){

}
