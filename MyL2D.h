/*
 * MyL2D.h
 *
 *  Created on: Aug 5, 2014
 *      Author: Nathan Steurs
 */

#ifndef MYL2D_H_
#define MYL2D_H_

#include "lparser.h"
#include <vector>
#include <stack>
#include "EasyImage.h"
#include "line.h"
#include "ini_configuration.hh"

class Position {
public:
	Position(double x1, double y1, double angle);
	virtual ~Position();

	double getAngle() const {
		return angle;
	}

	double getX() const {
		return x;
	}

	double getY() const {
		return y;
	}

private:
	double x;
	double y;
	double angle;
};

class MyL2D: public LParser::LSystem2D {
public:
	MyL2D(std::istream &in);
	double DegToRad(double k);
	void AddLine(double a, double b, double c, double d, double e, double f);
	void Create();
	void Iterate(std::string s, int it);
	img::EasyImage DrawSystem();
	img::EasyImage ScaleSystem(int size, img::Color& c, img::Color& bgcolor);
	bool Exists(char& c);
	void PprintLines(std::vector<line> p);
	virtual ~MyL2D();
private:
	std::vector<line> lines;
	double xMin;
	double xMax;
	double yMin;
	double yMax;
	double my_x;
	double my_y;
	std::stack<Position> Brackets;
};

#endif /* MYL2D_H_ */
