/*
 * Figure.cc
 *
 *  Created on: Aug 9, 2014
 *      Author: Nathan Steurs
 */

#include "Figure.hh"
#include <cmath>
#include <string>
#include <iostream>
#include <sstream>

Matrix eyePointTrans(Vector3D eyepoint);

Figure::Figure() {
	// TODO Auto-generated constructor stub
	xangle = 0;
	yangle = 0;
	zangle = 0;
	scale = 1.0;
	center.resize(3);
	center[0] = 0.0;
	center[1] = 0.0;
	center[2] = 0.0;
}

Figure::~Figure() {
	// TODO Auto-generated destructor stub
}

Figure& Figure::operator=(Figure& f){
	faces = f.faces;
	points = f.getPoints();
	type = f.getType();
	return *this;
}

std::vector<Vector3D>& Figure::getPoints(){
	return points;
}

void Figure::Scale(double scale){
	Matrix Mat;
	Mat(1,1) = scale;
	Mat(2,2) = scale;
	Mat(3,3) = scale;
	for(auto &point : points){
		point = point * Mat;
	}
}

void Figure::Translate(Vector3D vector){
	Matrix Mat;
	Mat(4,1) = vector.x;
	Mat(4,2) = vector.y;
	Mat(4,3) = vector.z;
	for(auto &point : points){
		point = point * Mat;
	}

}

void Figure::Translate(){
	Matrix Mat;
	Mat(4,1) = center[0];
	Mat(4,2) = center[1];
	Mat(4,3) = center[2];
	for(auto &point : points){
		point = point * Mat;
	}

}

void Figure::RotateX(double angle){
	Matrix xMat;
	xMat(2,2) = std::cos(angle);
	xMat(2,3) = std::sin(angle);
	xMat(3,2) = std::sin(angle) * (-1);
	xMat(3,3) = std::cos(angle);
	for(auto &point : points){
		point = point * xMat;
	}
}

void Figure::RotateY(double angle){
	Matrix Mat;
	Mat(1,1) = std::cos(angle);
	Mat(1,3) = std::sin(angle) * (-1);
	Mat(3,1) = std::sin(angle);
	Mat(3,3) = std::cos(angle);
	for(auto &point : points){
		point = point * Mat;
	}
}

void Figure::RotateZ(double angle){
	Matrix Mat;
	Mat(1,1) = std::cos(angle);
	Mat(1,2) = std::sin(angle);
	Mat(2,1) = std::sin(angle) * (-1);
	Mat(2,2) = std::cos(angle);
	for(auto &point : points){
		point = point * Mat;
	}
}

void Figure::TransformEye(Vector3D eyepoint){
	Matrix m = eyePointTrans(eyepoint);
	for(auto &point : points){
		point = point * m;
	}
}

void Figure::GetInfoConf(const ini::Configuration& config, int figurenumber){
	std::string s = "Figure";
	s = s + std::to_string(figurenumber);
	//long long int nrPoints = config[s]["nrPoints"].as_int_or_die();
	int nrPoints = config[s]["nrPoints"].as_int_or_default(0);
	//long long int nrLines = config[s]["nrLines"].as_int_or_die();
	int nrLines = config[s]["nrLines"].as_int_or_default(0);
	std::vector<double> figcol = config[s]["color"].as_double_tuple_or_die();
	type = config[s]["type"].as_string_or_die();
	xangle = config[s]["rotateX"].as_double_or_die();
	yangle = config[s]["rotateY"].as_double_or_die();
	zangle = config[s]["rotateZ"].as_double_or_die();
	center = config[s]["center"].as_double_tuple_or_die();
	scale = config[s]["scale"].as_double_or_die();
	std::vector<double> kleur = config[s]["color"].as_double_tuple_or_die();
	color = drawing::GetColour(kleur);
	ToRadAngles();

	if(type == "LineDrawing"){
		std::cout << "Reading your LineDrawing and extracting points and lines." << std::endl;
		if(nrPoints > 0){
			for(int i = 0; i < nrPoints; i++){
				std::string temp = "point";
				temp = temp + std::to_string(i);
				std::vector<double> tpoint = config[s][temp].as_double_tuple_or_die();
				Vector3D p = Vector3D::point(tpoint[0],tpoint[1],tpoint[2]);
				points.push_back(p);
			}
		}
		if(nrLines > 0){
			faces.resize(nrLines);
			for(int j = 0; j < nrLines; j++){
				std::string ltemp = "line";
				ltemp = ltemp + std::to_string(j);
				std::vector<int> line = config[s][ltemp].as_int_tuple_or_die();
				faces[j].point_indexes.push_back(line[0]);
				faces[j].point_indexes.push_back(line[1]);
			}
		}
	}
	else{
		std::cout << type << " Number: " << figurenumber << " Was Generated!" << std::endl;
		//Points/faces are assigned in assignment
	}

}
std::vector<line>& Figure::getLines(){
	return lines;
}
void Figure::GenerateLines(){
	//TODO make this into one simple method
	if(type == "LineDrawing"){
		for(auto face : faces){
			//Todo this works for linedrawing only
			double x1 = points[face.point_indexes[0]].x;
			double y1 = points[face.point_indexes[0]].y;
			double z1 = points[face.point_indexes[0]].z;
			double x2 = points[face.point_indexes[1]].x;
			double y2 = points[face.point_indexes[1]].y;
			double z2 = points[face.point_indexes[1]].z;
			AddLine(x1, y1, z1, x2, y2, z2, color);
		}
	}
	else{
		for(auto face : faces){
			auto s = face.point_indexes.size();
			for(int t = 0; t < s; t++){
				double x1 = points[face.point_indexes[t]].x;
				double y1 = points[face.point_indexes[t]].y;
				double z1 = points[face.point_indexes[t]].z;
				double x2 = points[face.point_indexes[(t+1) % s]].x;
				double y2 = points[face.point_indexes[(t+1) % s]].y;
				double z2 = points[face.point_indexes[(t+1) % s]].z;
				AddLine(x1, y1, z1, x2, y2, z2, color);
			}

		}
	}
}

void Figure::Project(){
	for(auto &point : points){
		if(point.z != 0){
			point.x = point.x / (- point.z);
			point.y = point.y / (- point.z);
		}

	}
}

void Figure::ToRadAngles(){
	xangle = xangle*M_PI/180;
	yangle = yangle*M_PI/180;
	zangle = zangle*M_PI/180;
}

void Figure::AddLine(line l){
	lines.push_back(l);
}

void Figure::AddLine(double x1, double y1, double z1, double x2, double y2, double z2, img::Color clr){
	line l(x1, x2, y1, y2, z1, z2, clr);
	lines.push_back(l);
}
// ##########"Helpers"##########

void ApplyTransformation(Figure& f, Matrix& m) {
	for(auto &point : f.getPoints()){
		point = point * m;
	}
}

Matrix TransMatrix(double theta, double phi, double r){
	Matrix Trans;
	Trans(1,1) = - std::sin(theta);
	Trans(1,2) = - (std::cos(theta) * std::cos(phi));
	Trans(1,3) = (std::cos(theta) * std::sin(phi));

	Trans(2,1) = std::cos(theta);
	Trans(2,2) = - (std::sin(theta) * std::cos(phi));
	Trans(2,3) = (std::sin(theta) * std::sin(phi));

	Trans(3,2) = std::sin(phi);
	Trans(3,3) = std::cos(phi);

	Trans(4,3) = -r;
	return Trans;
}

Matrix eyePointTrans(Vector3D eyepoint){
	float r = eyepoint.length();
	float theta = std::atan2(eyepoint.y, eyepoint.x);
	float phi = 0;
	if((r == 0)){
		phi = 0;
	}
	else{
		phi = std::acos(eyepoint.z/r);
	}
	Matrix trans = TransMatrix(theta, phi, r);
	return trans;
}
