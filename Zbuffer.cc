/*
 * Zbuffer.cc
 *
 *  Created on: Aug 17, 2014
 *      Author: Nathan Steurs
 */

#include "Zbuffer.hh"

Zbuffer::Zbuffer()
{
	// TODO Auto-generated constructor stub
}

Zbuffer::Zbuffer(int width, int height)
{
	double max = std::numeric_limits<double>::infinity();
	mybuffer.resize(width);
	for (int j = 0; j < width; j++) {
		mybuffer[j].resize(height);
	}
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			mybuffer[i][j] = max;
		}
	}
}

void Zbuffer::BufferLine(img::EasyImage& img, line l)
{
	/*
	 * Split into 3 cases, we have Horizontal line, Vertical line
	 * and skewed line
	 */
	int Xmax = 0;
	int Ymax = 0;
	int Zmax = 0;
	int Xmin = 0;
	int Ymin = 0;
	int Zmin = 0;
	//l.ShowFullObject();
	//Vertical line, where x values are equal.
	if ((l.getX1() == l.getX2()) /*|| std::fabs(l.getX2() - l.getX1()) < 0.0001*/) {
		//std::cout << "ENTRY VERTICAL LINE" << std::endl;
		//Line goes up
		if (l.getY1() < l.getY2()) {
			Ymin = l.getY1();
			Ymax = l.getY2();
			Zmin = l.getZ1();
			Zmax = l.getZ2();
		} else {
			//Line goes down
			Ymin = l.getY2();
			Ymax = l.getY1();
			Zmin = l.getZ2();
			Zmax = l.getZ1();
		}
		if(l.getZ1() < l.getZ2()){
			Zmin = l.getZ2();
			Zmax = l.getZ1();
		} else{
			Zmin = l.getZ1();
			Zmax = l.getZ2();
		}
		//int i = std::round(Ymin);
		//std::cout << "Value of i:" << i << std::endl;
		//std::cout << "Ends when i reaches:" << std::round(Ymax + 0.5) << std::endl;
		for (int i = std::round(Ymin); i <= std::round(Ymax); i++) {
			//BufferPoint(...);
			//Point + points of line they are on

			// this->BufferPoint(l.getX1(), i, l.getX1(), Ymin, Zmin, l.getX1(), Ymax, Zmax, l.getColor(),
			//         img);
			this->BufferPoint(l.getX1(), i, l.getX1(), Ymin, Zmin, l.getX1(), Ymax, Zmax, l.getColor(),
			        img);
			//Check the Z buffer
		}
	}
	//Horizontal Line2
	else if ((l.getY1() == l.getY2())/* || std::fabs(l.getY2() - l.getY1()) < 0.00001*/) {
		//	std::cout << "ENTRY HORIZONTAL LINE" << std::endl;
		if (l.getX1() < l.getX2()) {
			Xmin = l.getX1();
			Xmax = l.getX2();
			Zmin = l.getZ1();
			Zmax = l.getZ2();
		} else {
			Xmin = l.getX2();
			Xmax = l.getX1();
			Zmin = l.getZ2();
			Zmax = l.getZ1();
		}

		if(l.getZ1() < l.getZ2()){
			Zmin = l.getZ2();
			Zmax = l.getZ1();
		} else{
			Zmin = l.getZ1();
			Zmax = l.getZ2();
		}
		//int i = std::round(Xmin);
		//std::cout << "Value of i:" << i << std::endl;
		//std::cout << "Ends when i reaches:" << std::round(Xmax + 0.5) << std::endl;
		for (int i = std::round(Xmin + 0.5); i <= std::round(Xmax + 0.5); i++) {

			this->BufferPoint(i, l.getY1(), Xmin, l.getY1(), Zmin, Xmax, l.getY1(), Zmax, l.getColor(),
			        img);
		}
	}
	//Skewed line
	//We have to calculate the slope
	else {
		//std::cout << "ENTERING MAIN ELSE BLOCK" << std::endl;
		if (l.getX2() < l.getX1()) {
			//line newline = new line(l.getX2(), l.getX1(), l.getY2(), l.getY1(), l.getZ2(), l.getZ1(), l.getColor());
			l = line(l.getX2(), l.getX1(), l.getY2(), l.getY1(), l.getZ2(), l.getZ1(), l.getColor());
		}

		long double slope = 0;
		long double teller = 0;
		long double noemer = 0;

		teller = l.getY2() - l.getY1();
		if (std::fabs(teller) < 0.001)
			teller = 0;

		noemer = l.getX2() - l.getX1();
		if (std::fabs(noemer) < 0.001)
			noemer = 0;

		//std::cout << "Teller:" << teller << std::endl;
		//std::cout << "Noemer:" << noemer << std::endl;

		slope = teller / noemer;
		//	if(slope > 0){
		//		if(std::trunc(slope + 0.0001) > std::trunc(slope)){
		//		slope = std::trunc(slope + 0.0001);
		//		}
		//	}

		//slope = std::round(slope);

		//std::cout << "slope:" << slope << std::endl;

		//BufferPoint(double x1, double y1, double xa, double ya, double za, double xb, double yb, double zb,
		        //img::Color c, img::EasyImage& img)

		if ((-1.0 <= slope) && (1.0 >= slope)) {
			//	std::cout << "ENTRY TUSSEN:"<< std::round(l.getX2() + 0.5) - std::round(l.getX1() + 0.5) << std::endl;

			for (int i = 0; i <= std::round(l.getX2()) - std::round(l.getX1()); i++) {

				this->BufferPoint(l.getX1() + i, l.getY1() + (slope * i), l.getX1(), l.getY1(),
				        l.getZ1(), l.getX2(), l.getY2(), l.getZ2(), l.getColor(), img);
			}
		} else if (slope < -1.0) {
			//	std::cout << "ENTRY LT -1" << std::endl;
			for (int i = 0; i <= std::round(l.getY1()) - std::round(l.getY2()); i++) {

				this->BufferPoint(l.getX1() - (i / slope), l.getY1() - i, l.getX1(), l.getY1(),
				        l.getZ1(), l.getX2(), l.getY2(), l.getZ2(), l.getColor(), img);
			}
		} else if (slope > 1.0) {
			//	std::cout << "ENTRY GT 1" << std::endl;
			for (int i = 0; i <= std::round(l.getY2()) - std::round(l.getY1()); i++) {

				this->BufferPoint(l.getX1() + (i / slope), l.getY1() + i, l.getX1(), l.getY1(),
				        l.getZ1(), l.getX2(), l.getY2(), l.getZ2(), l.getColor(), img);
			}

		} else {
		//	std::cout << "Woops, slope is the magical number:" << slope << std::endl;
		//	std::cout << "Teller:" << teller << std::endl;
		//	std::cout << "Noemer:" << noemer << std::endl;
		}
	}

}

void Zbuffer::BufferPoint(double x1, double y1, double xa, double ya, double za, double xb, double yb, double zb,
        img::Color c, img::EasyImage& img)
{

	//std::cout << "Entering Bufferpoint" << std::endl;

	double p = 0;
	double Zi = 0;

	p = this->P(x1, y1, xa, xb, ya, yb);
	Zi = this->Zi(p, za, zb);

	//std::cout << "[P=" << p << "][Zi=" << Zi << "]" << std::endl;
	if (Zi < this->mybuffer[std::round(x1)][std::round(y1)]) {
		this->mybuffer[std::round(x1)][std::round(y1)] = Zi;
		img(std::round(x1), std::round(y1)) = c;
		//std::cout << "recoloring pixel" << std::endl;
	} else {
		//std::cout << "No need to recolor" << std::endl;
	}

}

double Zbuffer::P(double x1, double y1, double xa, double xb, double ya, double yb)
{

	double px = (x1 - xb) / (xa - xb);
	double py = (y1 - yb) / (ya - yb);
	//std::cout << "PX PY" << px << " " << py << std::endl;
	if ((px <= 1) && (px >= 0)) {
		return px;
	} else if ((py <= 1) && (py >= 0)) {
		return py; // was py
	} else {
		if ((px < 0) || (py < 0)) {
			return 0;
		} else {
			return 1;
		}

	}
	/*
	 * x1 = p.xa + xb - p.xb
	 * px = (x1-xb)/(xa-xb)
	 * y1 = p.ya + yb - p.yb
	 * py = (y1 - yb) / (ya - yb)
	 */
}

double Zbuffer::Zi(double p, double za, double zb)
{
	double rval = 0;
	rval = (p / za) + ((1 - p) / zb);
	return rval;
}

void Zbuffer::Draw(img::EasyImage& img, std::vector<line> lines)
{
	for (auto& line : lines) {
		this->BufferLine(img, line);
	}
}

void Zbuffer::T_draw_zbuf(std::vector<Figure> &f, img::EasyImage& img, double d, double dx, double dy)
{
	/*
	 * Find all points for the Triangle, and call the drawing procedure.
	 */
	for (auto figure : f) {
		for (unsigned int i = 0; i < figure.getFaces().size(); i++) {
			std::vector<Vector3D>& p = figure.getPoints();
			const std::vector<Face>& faces = figure.getFaces();
			auto A = Vector3D::point(p[faces[i].point_indexes[0]].x, p[faces[i].point_indexes[0]].y,
			        p[faces[i].point_indexes[0]].z);
			auto B = Vector3D::point(p[faces[i].point_indexes[1]].x, p[faces[i].point_indexes[1]].y,
			        p[faces[i].point_indexes[1]].z);
			auto C = Vector3D::point(p[faces[i].point_indexes[2]].x, p[faces[i].point_indexes[2]].y,
			        p[faces[i].point_indexes[2]].z);
			//std::cout << i << std::endl;
			T_draw_zbuf_triag(img, A, B, C, d, dx, dy, figure.getColor());
		}
	}
}

void Zbuffer::T_draw_zbuf_triag(img::EasyImage& img, Vector3D& a, Vector3D& b, Vector3D& c, double d, double dx, double dy,
        img::Color color)
{
	//std::cout << "d:" << d << std::endl;
	//std::cout << "dx:" << dx << std::endl;
	//std::cout << "Aold: " << a*d << std::endl;
	//std::cout << "Bold: " << b*d << std::endl;
	//std::cout << "Cold: " << c*d << std::endl;
	//A B and C are already projected!

	auto anew = Vector3D::point( a.x *(-a.z) ,  a.y*(-a.z) , a.z);//Take the points that are NOT projected
	auto bnew = Vector3D::point( b.x*(-b.z) ,  b.y*(-b.z) , b.z);
	auto cnew = Vector3D::point( c.x*(-c.z) ,  c.y*(-c.z) , c.z);

	//std::cout << "A: "<< anew.x << "|" << anew.y << "|"<< anew.z<< "|" << std::endl;
	//std::cout << "B:" << bnew.x << "|" << bnew.y << "|"<< bnew.z<< "|" << std::endl;
	//std::cout << "C:"<< cnew.x << "|" << cnew.y << "|"<< cnew.z<< "|" << std::endl;

	Vector3D u = Vector3D::vector(bnew - anew);
	Vector3D v = Vector3D::vector(cnew - anew);
	Vector3D w = Vector3D::cross(u, v);

	//std::cout << "u:" << u.x << "|" << u.y << "|"<< u.z<< "|" << std::endl;
	//std::cout << "v:"<< v.x << "|" << v.y << "|"<< v.z<< "|" << std::endl;
	//std::cout << "w:"<< w.x << "|" << w.y << "|"<< w.z<< "|" << std::endl;
	a = a*d;
	b = b*d;
	c = c*d;


	Vector3D g = Vector3D::point((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3,
	        (1 / (anew.z * 3)) + (1 / (bnew.z * 3)) + (1 / (cnew.z * 3)));
	//Gx, Gy, 1/Gz
	//std::cout << "G:"<< g << std::endl;

	long double k = (w.x * anew.x) + (w.y * anew.y) + (w.z * anew.z);//changed to A values
	//std::cout << "k:" << k << std::endl;

	long double dzdx = w.x / -(d * k);
	long double dzdy = w.y / -(d * k);
	//std::cout << "dzdx:" << dzdx << "/dzdy:" << dzdy << std::endl;
	std::vector<long double> yminv { a.y, b.y, c.y };

	long double ymin = *std::min_element(std::begin(yminv), std::end(yminv));
	long double ymax = *std::max_element(std::begin(yminv), std::end(yminv));

	//std::cout << "ymin:" << ymin << std::endl;
	//std::cout << "ymax:" << ymax << std::endl;
	double Xl = 0;
	double Xr = 0;
	for (int i = std::round(ymin); i <= std::round(ymax); i++) {
		//Voor elke Y waarde tussen min en max
		Xl = 0;
		Xr = 0;
		auto idy = i+dy;
		long double Zi = 0.0;
		//std::cout << "Inbound" << std::endl;
		if (T_GetXlXr(a, b, c, Xl, Xr, i)) {
			//The point we want to draw is within bounds, so we can draw
			//std::cout << "xl:" << Xl << "|" << "xr:" << Xr  << "i:" << i<< std::endl;
			Xl = std::round(Xl);
			Xr = std::round(Xr);
			//std::cout << "xl:" << Xl << "|" << "xr:" << Xr  << "i:" << i<< std::endl;
			for (int j = Xl; j <= Xr; j++) {
				auto jdx = j + dx;
				Zi = (1.0001 * (g.z)) + ((j - g.x) * dzdx) + ((i - g.y) * dzdy);
				//std::cout << "Z:" << 1/Zi << std::endl;
				//std::cout << "j:" << j << "|dzdx:" << dzdx << std::endl;
				if (Zi < this->mybuffer[jdx][idy]) {
					//std::cout << "Writing to buffer" << std::endl;
					mybuffer[jdx][idy] = Zi;
					img(jdx, idy) = color;
				}
			}
		}
	}
}

bool Zbuffer::T_GetXlXr(Vector3D& a, Vector3D& b, Vector3D& c, double &Xl, double &Xr, int yi)
{
	/*
	 * Initialize at infinity
	 */
	auto xlAB = std::numeric_limits<double>::infinity();
	auto xlAC = std::numeric_limits<double>::infinity();
	auto xlBC = std::numeric_limits<double>::infinity();
	auto xrAB = -std::numeric_limits<double>::infinity();
	auto xrAC = -std::numeric_limits<double>::infinity();
	auto xrBC = -std::numeric_limits<double>::infinity();

	// line AB = PQ
	if ((((yi - a.y) * (yi - b.y)) <= 0) && (a.y != b.y)) {
		xlAB = b.x + ((a.x - b.x) * ((yi - b.y) / (a.y - b.y)));
		xrAB = xlAB;
	}

	// // line AC
	if ((((yi - a.y) * (yi - c.y)) <= 0) && (a.y != c.y)) {
		xlAC = c.x + ((a.x - c.x) * ((yi - c.y) / (a.y - c.y)));
		xrAC = xlAC;
	}

	// // line BC
	if ((((yi - b.y) * (yi - c.y)) <= 0) && (b.y != c.y)) {
		xlBC = c.x + ((b.x - c.x) * ((yi - c.y) / (b.y - c.y)));
		xrBC = xlBC;
	}

	/*	if((xlAC == xlBC)&& (xlBC == xlAB)){
	 std::cout << "Yi:" << yi << std::endl;
	 std::cout << "First Test:"  << (yi-a.y) * (yi-b.y) << std::endl;
	 std::cout << "Second Test:" << (yi-a.y) * (yi-c.y) << std::endl;
	 std::cout << "Third Test:"  << (yi-b.y) * (yi-c.y) << std::endl;
	 a.print(std::cout << std::endl);
	 b.print(std::cout << std::endl);
	 c.print(std::cout << std::endl);

	 }
	 */
//	std::cout << xlAB << ";" << xlAC << ";" << xlBC << std::endl;
//	std::cout << xrAB << ";" << xrAC << ";" << xrBC << std::endl;
	std::vector<double> min { xlAB, xlAC, xlBC };
	std::vector<double> max { xrAB, xrAC, xrBC };

	double maximum = *max_element(std::begin(max), std::end(max));
	double minimum = *min_element(std::begin(min), std::end(min));

	Xl = minimum + 0.5;

	Xr = maximum - 0.5;
	//drawing::PrettyPrint(max);
	//drawing::PrettyPrint(min);
	return true;
}

Zbuffer::~Zbuffer()
{
	// TODO Auto-generated destructor stub
}
