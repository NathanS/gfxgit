/*
 * line.h
 *
 *  Created on: Aug 5, 2014
 *      Author: Nathan Steurs
 */

#ifndef LINE_H_
#define LINE_H_

#include "EasyImage.h"

class line {
public:
	line();
	line(double a, double b, double c, double d, double e, double f, img::Color clr);
	line(double a, double b, double c, double d, double e, double f);
	line(double a, double b, double c, double d);
	line(double a, double b, double c, double d, img::Color clr);
	void Show();
	void ShowFullObject();
	img::Color& getColor();
	virtual ~line();

	double getX1() const {
		return x1;
	}

	void setX1(double x1) {
		this->x1 = x1;
	}

	double getX2() const {
		return x2;
	}

	void setX2(double x2) {
		this->x2 = x2;
	}

	double getY1() const {
		return y1;
	}

	void setY1(double y1) {
		this->y1 = y1;
	}

	double getY2() const {
		return y2;
	}

	void setY2(double y2) {
		this->y2 = y2;
	}

	double getZ1() const {
		return z1;
	}

	void setZ1(double z1) {
		this->z1 = z1;
	}

	double getZ2() const {
		return z2;
	}

	void setZ2(double z2) {
		this->z2 = z2;
	}

private:
	double x1;
	double y1;
	double z1;
	double x2;
	double y2;
	double z2;
	img::Color color;
};

#endif /* LINE_H_ */
