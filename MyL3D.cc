/*
 * MyL3D.cpp
 *
 *  Created on: Aug 17, 2014
 *      Author: Nathan Steurs
 */

#include "MyL3D.hh"

MyL3D::MyL3D(std::istream &in) : LParser::LSystem3D(in) {
	// TODO Auto-generated constructor stub
	my_x = 0;
	my_y = 0;
	my_z = 0;
	H = Vector3D::point(1,0,0);
	L = Vector3D::point(0,1,0);
	U = Vector3D::point(0,0,1);
}

MyL3D::~MyL3D() {
	// TODO Auto-generated destructor stub
}
void MyL3D::GenPoints(){
	for(auto line : lsyslines){
		points.push_back(Vector3D::point(line.getX1(), line.getY1(), line.getZ1()));
		points.push_back(Vector3D::point(line.getX2(), line.getY2(), line.getZ2()));
	}

}

void MyL3D::SetCenter(std::vector<double> newcenter){
	this->center = newcenter;
}
void MyL3D::SetColor(img::Color newcolor){
	this->color = newcolor;
}
void MyL3D::GenLines(){
	for(int i = 0; i < points.size(); i++){
		if((i+1) < points.size()){
		line l(points[i].x,points[i+1].x, points[i].y, points[i+1].y, points[i].z, points[i+1].z, color);
		lines.push_back(l);
		}
	}
}
bool MyL3D::Exists(char& c){
	for(const char& k : alphabet){
		if((c == k)&&(drawfunction[c] == true)){
			return true;
		}
	}
	return false;
}

double MyL3D::DegToRad(double angle){
	return ((M_PI/180)*angle);
}

void MyL3D::AddLine(double x1, double y1, double z1, double x2, double y2, double z2){
	line l(x1,x2,y1,y2,z1,z2);
	lsyslines.push_back(l);
}

double MyL3D::getAngle(){
	return this->angle;
}

void MyL3D::Iterate(std::string rule, int iterations){
	for(auto c : rule){
		if(c == '+'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tl = L;
			H = th*std::cos(a) + tl*std::sin(a);
			L = - std::sin(a)*th + tl*std::cos(a);
		}
		else if(c == '-'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tl = L;
			H = th*std::cos(-a) + tl*std::sin(-a);
			L = - std::sin(-a)*th + tl*std::cos(-a);
		}
		else if(c == '^'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tu = U;
			H = th*std::cos(a) + tu*std::sin(a);
			U = - std::sin(a)*th + tu*std::cos(a);
		}
		else if(c == '&'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tu = U;
			H = th*std::cos(-a) + tu*std::sin(-a);
			U = - std::sin(-a)*th + tu*std::cos(-a);
		}
		else if(c == '/'){
			double a = DegToRad(angle);
			Vector3D tl = L;
			Vector3D tu = U;
			L = tl*std::cos(-a) - tu*sin(-a);
			U = tl*sin(-a) + tu*cos(-a);
		}
		else if(c == '\\'){
			double a = DegToRad(angle);
			Vector3D tl = L;
			Vector3D tu = U;
			L = tl*std::cos(a) - tu*sin(a);
			U = tl*sin(a) + tu*cos(a);
		}
		else if(c == '|'){
			H = -H;
			L = -L;
		}
		else if((c == '(')||(c == '[')||(c == '{')){
			Position3D p(my_x, my_y, my_z, H, L, U);
			Brackets.push(p);
		}
		else if((c == ')')||(c == ']')||(c == '}')){
			my_x = Brackets.top().getX();
			my_y = Brackets.top().getY();
			my_z = Brackets.top().getZ();
			H = Brackets.top().getH();
			L = Brackets.top().getL();
			U = Brackets.top().getU();
			Brackets.pop();
		}
		else if(Exists(c)){
			auto str = get_replacement(c);
			if(iterations == 1){
				double x2 = my_x + H.x;
				double y2 = my_y + H.y;
				double z2 = my_z + H.z;
				//std::cout << "Pushing line: " << my_x << my_y << my_z << std::endl << x2 << y2 << z2 << std::endl;
				AddLine(my_x, my_y, my_z, x2, y2, z2);
				my_x = x2;
				my_y = y2;
				my_z = z2;
			}
			else if(iterations > 1){
				Iterate(get_replacement(c), iterations-1);
			}
		}
	}
}
void MyL3D::To2D(){
//	points = GenPoints();
	Scale(getScale());
	RotateX(xangle);
	RotateY(yangle);
	RotateZ(zangle);
	Translate();
	//TransformEye();
	Project();
}

void MyL3D::Create(){
	for(auto c : initiator){
		if(c == '+'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tl = L;
			H = th*std::cos(a) + tl*std::sin(a);
			L = - std::sin(a)*th + tl*std::cos(a);
		}
		else if(c == '-'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tl = L;
			H = th*std::cos(-a) + tl*std::sin(-a);
			L = - std::sin(-a)*th + tl*std::cos(-a);
		}
		else if(c == '^'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tu = U;
			H = th*std::cos(a) + tu*std::sin(a);
			U = - std::sin(a)*th + tu*std::cos(a);
		}
		else if(c == '&'){
			double a = DegToRad(angle);
			Vector3D th = H;
			Vector3D tu = U;
			H = th*std::cos(-a) + tu*std::sin(-a);
			U = - std::sin(-a)*th + tu*std::cos(-a);
		}
		else if(c == '/'){
			double a = DegToRad(angle);
			Vector3D tl = L;
			Vector3D tu = U;
			L = tl*std::cos(-a) - tu*sin(-a);
			U = tl*sin(-a) + tu*cos(-a);
		}
		else if(c == '\\'){
			double a = DegToRad(angle);
			Vector3D tl = L;
			Vector3D tu = U;
			L = tl*std::cos(a) - tu*sin(a);
			U = tl*sin(a) + tu*cos(a);
		}
		else if(c == '|'){
			H = -H;
			L = -L;
		}
		else if((c == '(')||(c == '[')||(c == '{')){
			Position3D p(my_x, my_y, my_z, H, L, U);
			Brackets.push(p);
		}
		else if((c == ')')||(c == ']')||(c == '}')){
			my_x = Brackets.top().getX();
			my_y = Brackets.top().getY();
			my_z = Brackets.top().getZ();
			H = Brackets.top().getH();
			L = Brackets.top().getL();
			U = Brackets.top().getU();
			Brackets.pop();
		}
		else if(Exists(c)){
			auto str = get_replacement(c);
			if(nrIterations > 0){
				Iterate(str, nrIterations);
			}
			else{
				Iterate(str, 1);
			}
		}
		else{
			std::cout << "Encountered unknown symbol!" << std::endl;
		}
	}
}
/*
 * POSITION CLASS
 *
 */
Position3D::Position3D(double _x, double _y, double _z, Vector3D _H, Vector3D _L, Vector3D _U){
	x = _x;
	y = _y;
	z = _z;
	H = _H;
	L = _L;
	U = _U;
}

Position3D::~Position3D(){

}

