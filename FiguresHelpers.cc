/*
 * FiguresHelpers.cc
 *
 *  Created on: Aug 16, 2014
 *      Author: Nathan Steurs
 */

#include "FiguresHelpers.hh"
#include <ostream>
#include <sstream>

void FigureHelp::AssignCorrectType(Figure& f, const ini::Configuration& config, int fignr){
	if(f.getType() == "Cube"){
		Figures::Cube c;
		f = c;
	}
	else if(f.getType() == "Tetrahedron"){
		Figures::Tetrahedron t;
		f = t;
	}
	else if(f.getType() == "Octahedron"){
		Figures::Octahedron o;
		f = o;
	}
	else if(f.getType() == "Icosahedron"){
		Figures::Icosahedron i;
		f = i;
	}
	else if(f.getType() == "Dodecahedron"){
		Figures::Dodecahedron d;
		f = d;
	}
	else if(f.getType() == "Sphere"){
		Figures::Sphere s;
		std::string str = "Figure" + std::to_string(fignr);
		int n = config[str]["n"].as_int_or_default(1);
		s.Create(n);
		f = s;
	}
	else if(f.getType() == "Cone"){
		Figures::Cone c;
		std::string str = "Figure" + std::to_string(fignr);
		int height = config[str]["height"].as_double_or_default(1.0);
		int n = config[str]["n"].as_int_or_default(1);
		c.Create(n,height);
		f = c;
	}
	else if(f.getType() == "Cylinder"){
		Figures::Cylinder c;
		std::string str = "Figure" + std::to_string(fignr);
		int height = config[str]["height"].as_double_or_default(1.0);
		int n = config[str]["n"].as_int_or_default(1);
		c.Create(n,height);
		f = c;
	}
	else if(f.getType() == "Torus"){
		Figures::Torus t;
		std::string str = "Figure" + std::to_string(fignr);
		double r = config[str]["r"].as_double_or_die();
		double R = config[str]["R"].as_double_or_die();
		int m = config[str]["m"].as_int_or_die();
		int n = config[str]["n"].as_int_or_die();
		t.Create(n, m, r, R);
		f = t;
	}
	else if(f.getType() == "3DLSystem"){

	}
	else if(f.getType() == "LineDrawing"){
		std::cout << "Rotating, Scaling, Projecting, Drawing." << std::endl;
	}
	else{
		std::cerr << "Something went wrong while creating your 3D Image." << std::endl;
	}
}
