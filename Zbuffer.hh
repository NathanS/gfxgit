/*
 * Zbuffer.hh
 *
 *  Created on: Aug 17, 2014
 *      Author: Nathan Steurs
 */

#ifndef ZBUFFER_HH_
#define ZBUFFER_HH_

#include <cmath>
#include <limits>
#include "EasyImage.h"
#include "line.h"
#include "Figure.hh"
#include <algorithm>

class Zbuffer {
public:
	Zbuffer();
	Zbuffer(int width, int height);
	void BufferLine(img::EasyImage& img, line l);
	void BufferPoint(double x1, double y1, double xa, double ya, double za, double xb, double yb, double zb, img::Color c, img::EasyImage& img);
	double P(double x1, double y1, double xa, double xb, double ya, double yb);
	double Zi(double p, double za, double zb);
	void Draw(img::EasyImage& img, std::vector<line> lines);
	void T_draw_zbuf(std::vector<Figure> &f, img::EasyImage& img, double d, double dx, double dy);
	void T_draw_zbuf_triag(img::EasyImage& img, Vector3D& a, Vector3D& b, Vector3D& c, double d, double dx, double dy, img::Color clr);
	bool T_GetXlXr(Vector3D& a, Vector3D& b, Vector3D& c, double &Xl, double &Xr, int yi);
	virtual ~Zbuffer();
private:
	std::vector<std::vector<long double> > mybuffer;
};

#endif /* ZBUFFER_HH_ */
