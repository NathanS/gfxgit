/*
 * Figures.cc
 *
 *  Created on: Aug 16, 2014
 *      Author: Nathan Steurs
 */

#include "Figures.hh"
#include <cmath>

namespace Figures {

Cube::Cube()
{
	this->setType("Cube");
	points.push_back(Vector3D::point(1, -1, -1));
	points.push_back(Vector3D::point(-1, 1, -1));
	points.push_back(Vector3D::point(1, 1, 1));
	points.push_back(Vector3D::point(-1, -1, 1));
	points.push_back(Vector3D::point(1, 1, -1));
	points.push_back(Vector3D::point(-1, -1, -1));
	points.push_back(Vector3D::point(1, -1, 1));
	points.push_back(Vector3D::point(-1, 1, 1));
	faces.resize(6);
	faces[0].point_indexes.push_back(0);
	faces[0].point_indexes.push_back(4);
	faces[0].point_indexes.push_back(2);
	faces[0].point_indexes.push_back(6);
	faces[1].point_indexes.push_back(4);
	faces[1].point_indexes.push_back(1);
	faces[1].point_indexes.push_back(7);
	faces[1].point_indexes.push_back(2);
	faces[2].point_indexes.push_back(1);
	faces[2].point_indexes.push_back(5);
	faces[2].point_indexes.push_back(3);
	faces[2].point_indexes.push_back(7);
	faces[3].point_indexes.push_back(5);
	faces[3].point_indexes.push_back(0);
	faces[3].point_indexes.push_back(6);
	faces[3].point_indexes.push_back(3);
	faces[4].point_indexes.push_back(6);
	faces[4].point_indexes.push_back(2);
	faces[4].point_indexes.push_back(7);
	faces[4].point_indexes.push_back(3);
	faces[5].point_indexes.push_back(0);
	faces[5].point_indexes.push_back(5);
	faces[5].point_indexes.push_back(1);
	faces[5].point_indexes.push_back(4);

}

Cube::~Cube()
{

}

Tetrahedron::Tetrahedron()
{
	setType("Tetrahedron");
	points.push_back(Vector3D::point(1, -1, -1));
	points.push_back(Vector3D::point(-1, 1, -1));
	points.push_back(Vector3D::point(1, 1, 1));
	points.push_back(Vector3D::point(-1, -1, 1));
	faces.resize(4);
	faces[0].point_indexes.push_back(0);
	faces[0].point_indexes.push_back(1);
	faces[0].point_indexes.push_back(2);

	faces[1].point_indexes.push_back(1);
	faces[1].point_indexes.push_back(3);
	faces[1].point_indexes.push_back(2);

	faces[2].point_indexes.push_back(0);
	faces[2].point_indexes.push_back(3);
	faces[2].point_indexes.push_back(1);

	faces[3].point_indexes.push_back(0);
	faces[3].point_indexes.push_back(2);
	faces[3].point_indexes.push_back(3);
}

Octahedron::Octahedron()
{
	setType("Octahedron");
	points.push_back(Vector3D::point(1, 0, 0));
	points.push_back(Vector3D::point(0, 1, 0));
	points.push_back(Vector3D::point(-1, 0, 0));
	points.push_back(Vector3D::point(0, -1, 0));
	points.push_back(Vector3D::point(0, 0, -1));
	points.push_back(Vector3D::point(0, 0, 1));
	faces.resize(8);
	faces[0].point_indexes.push_back(0);
	faces[0].point_indexes.push_back(1);
	faces[0].point_indexes.push_back(5);
	faces[1].point_indexes.push_back(1);
	faces[1].point_indexes.push_back(2);
	faces[1].point_indexes.push_back(5);
	faces[2].point_indexes.push_back(2);
	faces[2].point_indexes.push_back(3);
	faces[2].point_indexes.push_back(5);
	faces[3].point_indexes.push_back(3);
	faces[3].point_indexes.push_back(0);
	faces[3].point_indexes.push_back(5);
	faces[4].point_indexes.push_back(1);
	faces[4].point_indexes.push_back(0);
	faces[4].point_indexes.push_back(4);
	faces[5].point_indexes.push_back(2);
	faces[5].point_indexes.push_back(1);
	faces[5].point_indexes.push_back(4);
	faces[6].point_indexes.push_back(3);
	faces[6].point_indexes.push_back(2);
	faces[6].point_indexes.push_back(4);
	faces[7].point_indexes.push_back(0);
	faces[7].point_indexes.push_back(3);
	faces[7].point_indexes.push_back(4);
}
Icosahedron::Icosahedron()
{
	setType("Icosahedron");
	points.push_back(Vector3D::point(0, 0, (std::sqrt(5) / 2)));
	for (int i = 2; i < 7; i++) {
		points.push_back(
		        Vector3D::point(std::cos((i - 2) * 2 * M_PI / 5), std::sin((i - 2) * 2 * M_PI / 5), 0.5));
	}
	for (int j = 7; j < 12; j++) {
		points.push_back(
		        Vector3D::point(std::cos((M_PI / 5) + ((j - 7) * 2 * M_PI / 5)),
		                std::sin((M_PI / 5) + ((j - 7) * 2 * M_PI / 5)), -0.5));
	}
	points.push_back(Vector3D::point(0, 0, -std::sqrt(5) / 2));
	faces.resize(20);
	faces[0].point_indexes.push_back(0);
	faces[0].point_indexes.push_back(1);
	faces[0].point_indexes.push_back(2);
	faces[1].point_indexes.push_back(0);
	faces[1].point_indexes.push_back(2);
	faces[1].point_indexes.push_back(3);
	faces[2].point_indexes.push_back(0);
	faces[2].point_indexes.push_back(3);
	faces[2].point_indexes.push_back(4);
	faces[3].point_indexes.push_back(0);
	faces[3].point_indexes.push_back(4);
	faces[3].point_indexes.push_back(5);
	faces[4].point_indexes.push_back(0);
	faces[4].point_indexes.push_back(5);
	faces[4].point_indexes.push_back(1);
	faces[5].point_indexes.push_back(1);
	faces[5].point_indexes.push_back(6);
	faces[5].point_indexes.push_back(2);
	faces[6].point_indexes.push_back(2);
	faces[6].point_indexes.push_back(6);
	faces[6].point_indexes.push_back(7);
	faces[7].point_indexes.push_back(2);
	faces[7].point_indexes.push_back(7);
	faces[7].point_indexes.push_back(3);
	faces[8].point_indexes.push_back(3);
	faces[8].point_indexes.push_back(7);
	faces[8].point_indexes.push_back(8);
	faces[9].point_indexes.push_back(3);
	faces[9].point_indexes.push_back(8);
	faces[9].point_indexes.push_back(4);
	faces[10].point_indexes.push_back(4);
	faces[10].point_indexes.push_back(8);
	faces[10].point_indexes.push_back(9);
	faces[11].point_indexes.push_back(4);
	faces[11].point_indexes.push_back(9);
	faces[11].point_indexes.push_back(5);
	faces[12].point_indexes.push_back(5);
	faces[12].point_indexes.push_back(9);
	faces[12].point_indexes.push_back(10);
	faces[13].point_indexes.push_back(5);
	faces[13].point_indexes.push_back(10);
	faces[13].point_indexes.push_back(1);
	faces[14].point_indexes.push_back(1);
	faces[14].point_indexes.push_back(10);
	faces[14].point_indexes.push_back(6);
	faces[15].point_indexes.push_back(11);
	faces[15].point_indexes.push_back(7);
	faces[15].point_indexes.push_back(6);
	faces[16].point_indexes.push_back(11);
	faces[16].point_indexes.push_back(8);
	faces[16].point_indexes.push_back(7);
	faces[17].point_indexes.push_back(11);
	faces[17].point_indexes.push_back(9);
	faces[17].point_indexes.push_back(8);
	faces[18].point_indexes.push_back(11);
	faces[18].point_indexes.push_back(10);
	faces[18].point_indexes.push_back(9);
	faces[19].point_indexes.push_back(11);
	faces[19].point_indexes.push_back(6);
	faces[19].point_indexes.push_back(10);
}

std::vector<Vector3D> Icosahedron::GenerateDodecahedronPoints()
{
	std::vector<Vector3D> newpoints;
	for (int i = 0; i < faces.size(); i++) {
		Vector3D v;
		for (int j = 0; j < 3; j++) {
			v.x += points[faces[i].point_indexes[j]].x;
			v.y += points[faces[i].point_indexes[j]].y;
			v.z += points[faces[i].point_indexes[j]].z;
		}
		v.x = v.x / 3;
		v.y = v.y / 3;
		v.z = v.z / 3;
		newpoints.push_back(v);
	}
	return newpoints;
}
Dodecahedron::Dodecahedron()
{
	setType("Dodecahedron");
	faces.resize(12);
	faces[0].point_indexes.push_back(0);
	faces[0].point_indexes.push_back(1);
	faces[0].point_indexes.push_back(2);
	faces[0].point_indexes.push_back(3);
	faces[0].point_indexes.push_back(4);
	faces[1].point_indexes.push_back(0);
	faces[1].point_indexes.push_back(5);
	faces[1].point_indexes.push_back(6);
	faces[1].point_indexes.push_back(7);
	faces[1].point_indexes.push_back(1);
	faces[2].point_indexes.push_back(1);
	faces[2].point_indexes.push_back(7);
	faces[2].point_indexes.push_back(8);
	faces[2].point_indexes.push_back(9);
	faces[2].point_indexes.push_back(2);
	faces[3].point_indexes.push_back(2);
	faces[3].point_indexes.push_back(9);
	faces[3].point_indexes.push_back(10);
	faces[3].point_indexes.push_back(11);
	faces[3].point_indexes.push_back(3);
	faces[4].point_indexes.push_back(3);
	faces[4].point_indexes.push_back(11);
	faces[4].point_indexes.push_back(12);
	faces[4].point_indexes.push_back(13);
	faces[4].point_indexes.push_back(4);
	faces[5].point_indexes.push_back(4);
	faces[5].point_indexes.push_back(13);
	faces[5].point_indexes.push_back(14);
	faces[5].point_indexes.push_back(5);
	faces[5].point_indexes.push_back(0);
	faces[6].point_indexes.push_back(19);
	faces[6].point_indexes.push_back(18);
	faces[6].point_indexes.push_back(17);
	faces[6].point_indexes.push_back(16);
	faces[6].point_indexes.push_back(15);
	faces[7].point_indexes.push_back(19);
	faces[7].point_indexes.push_back(14);
	faces[7].point_indexes.push_back(13);
	faces[7].point_indexes.push_back(12);
	faces[7].point_indexes.push_back(18);
	faces[8].point_indexes.push_back(18);
	faces[8].point_indexes.push_back(12);
	faces[8].point_indexes.push_back(11);
	faces[8].point_indexes.push_back(10);
	faces[8].point_indexes.push_back(17);
	faces[9].point_indexes.push_back(17);
	faces[9].point_indexes.push_back(10);
	faces[9].point_indexes.push_back(9);
	faces[9].point_indexes.push_back(8);
	faces[9].point_indexes.push_back(16);
	faces[10].point_indexes.push_back(16);
	faces[10].point_indexes.push_back(8);
	faces[10].point_indexes.push_back(7);
	faces[10].point_indexes.push_back(6);
	faces[10].point_indexes.push_back(15);
	faces[11].point_indexes.push_back(15);
	faces[11].point_indexes.push_back(6);
	faces[11].point_indexes.push_back(5);
	faces[11].point_indexes.push_back(14);
	faces[11].point_indexes.push_back(19);
	Icosahedron i;
	points = i.GenerateDodecahedronPoints();
}

Sphere::Sphere()
{
	setType("Sphere");
}

void Sphere::Create(int n)
{
	Icosahedron ico;
	for (int i = 0; i < n; i++) {
		Generate(ico);
	}
	points = ico.getPoints();
	faces = ico.getFaces();
}
void Sphere::Generate(Icosahedron& ico)
{
	std::vector<Vector3D> spoints;
	std::vector<Face> sfaces;
	sfaces.resize(4 * ico.getFaces().size());
	int t = 0;
	for (int i = 0; i < ico.getFaces().size(); i++) {
		Vector3D d;
		d.x = (ico.getPoints()[ico.getFaces()[i].point_indexes[0]].x
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[1]].x) / 2;
		//d.x = d.x / 2;
		d.y = (ico.getPoints()[ico.getFaces()[i].point_indexes[0]].y
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[1]].y)/ 2;
		//d.y = d.y / 2;
		d.z = (ico.getPoints()[ico.getFaces()[i].point_indexes[0]].z
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[1]].z)/ 2;
		//d.z = d.z / 2;
		Vector3D e;
		e.x = ico.getPoints()[ico.getFaces()[i].point_indexes[2]].x
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[0]].x;
		e.x = e.x / 2;
		e.y = ico.getPoints()[ico.getFaces()[i].point_indexes[2]].y
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[0]].y;
		e.y = e.y / 2;
		e.z = ico.getPoints()[ico.getFaces()[i].point_indexes[2]].z
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[0]].z;
		e.z = e.z / 2;
		Vector3D f;
		f.x = ico.getPoints()[ico.getFaces()[i].point_indexes[1]].x
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[2]].x;
		f.x = f.x / 2;
		f.y = ico.getPoints()[ico.getFaces()[i].point_indexes[1]].y
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[2]].y;
		f.y = f.y / 2;
		f.z = ico.getPoints()[ico.getFaces()[i].point_indexes[1]].z
		        + ico.getPoints()[ico.getFaces()[i].point_indexes[2]].z;
		f.z = f.z / 2;
		spoints.push_back(ico.getPoints()[ico.getFaces()[i].point_indexes[0]]);
		spoints.push_back(d);
		spoints.push_back(ico.getPoints()[ico.getFaces()[i].point_indexes[1]]);
		spoints.push_back(f);
		spoints.push_back(ico.getPoints()[ico.getFaces()[i].point_indexes[2]]);
		spoints.push_back(e);

		sfaces[t].point_indexes.push_back(6 * i);
		sfaces[t].point_indexes.push_back(6 * i + 1);
		sfaces[t].point_indexes.push_back(6 * i + 5);
		t++;
		sfaces[t].point_indexes.push_back(6 * i + 2);
		sfaces[t].point_indexes.push_back(6 * i + 3);
		sfaces[t].point_indexes.push_back(6 * i + 1);
		t++;
		sfaces[t].point_indexes.push_back(6 * i + 4);
		sfaces[t].point_indexes.push_back(6 * i + 5);
		sfaces[t].point_indexes.push_back(6 * i + 3);
		t++;
		sfaces[t].point_indexes.push_back(6 * i + 1);
		sfaces[t].point_indexes.push_back(6 * i + 3);
		sfaces[t].point_indexes.push_back(6 * i + 5);
		t++;
	}
	for (auto& point : spoints) {
		point.normalise();
	}
	ico.setFaces(sfaces);
	ico.setPoints(spoints);
}

Cone::Cone()
{
	setType("Cone");
}

void Cone::Create(int n, int height)
{
	for (int i = 0; i < n; i++) {
		points.push_back(Vector3D::point(std::cos(2 * i * M_PI / n), std::sin(2 * i * M_PI / n), 0));
	}
	points.push_back(Vector3D::point(0, 0, height));
	faces.resize(n + 1);
	int j = 0;
	for (auto& face : faces) {
		face.point_indexes.push_back(j);
		face.point_indexes.push_back((j + 1) % n);
		face.point_indexes.push_back(n);
		j++;
	}
	for (int i = n; i > 0; i--) {
		faces[n].point_indexes.push_back(n - i);
	}
}
Cylinder::Cylinder()
{
	setType("Cylinder");
}

void Cylinder::Create(int n, int height)
{
	for (int i = 0; i < n; i++) {
		//Bottom Points
		points.push_back(Vector3D::point(std::cos(2 * i * M_PI / n), std::sin(2 * i * M_PI / n), 0));
	}
	for (int i = 0; i < n; i++) {
		//TopPoints
		points.push_back(Vector3D::point(std::cos(2 * i * M_PI / n), std::sin(2 * i * M_PI / n), height));
	}
	faces.resize(n + 2); //n + Top + bottom
	for (int j = 0; j < n; j++) {
		//Enkel loopen over eerste n faces = die aande zijkant!
		//botl
		faces[j].point_indexes.push_back(j);
		//botr
		faces[j].point_indexes.push_back((j + 1) % n);
		//topR
		faces[j].point_indexes.push_back(((n + j + 1) % n) + n);
		//topL
		faces[j].point_indexes.push_back(n + j);
	}
	//top and bottom
	for (int i = n; i > 1; i--) {
		faces[n].point_indexes.push_back((n - i));

	}
	for (int i = n; i > 1; i--) {
		faces[n + 1].point_indexes.push_back(2 * n - i);
	}

}

Torus::Torus()
{

}

void Torus::Create(int n, int m, double r, double R)
{
	double u;
	double v;

	for (int i = 0; i < n ; i++) {
		for (int j = 0; j < m ; j++) {
			u = 2 * i * M_PI / n;
			v = 2 * j * M_PI / m;
			Vector3D p;
			p.x = ((R + r * std::cos(v)) * std::cos(u));
			p.y = ((R + r * std::cos(v)) * std::sin(u));
			p.z = r * std::sin(v);
			points.push_back(p);
		}
	}

	faces.resize(n * m);

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {

			faces[(i * m) + j].point_indexes.push_back(i*m + j);

			faces[(i * m) + j].point_indexes.push_back(((i+1)%n) * m + j);

			faces[(i * m) + j].point_indexes.push_back(((i+1)%n) * m + (j+1)%m);

			faces[(i * m) + j].point_indexes.push_back((i * m + (j+1)%m));
		}
	}
}

} /* namespace Figures */
