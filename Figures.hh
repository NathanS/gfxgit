/*
 * Figures.hh
 *
 *  Created on: Aug 16, 2014
 *      Author: Nathan Steurs
 */

#ifndef FIGURES_HH_
#define FIGURES_HH_

#include "Figure.hh"
#include "vector.hh"
#include <vector>
#include <cmath>

namespace Figures {

class Cube : public Figure {
public:
	Cube();
	virtual ~Cube();
private:
};

class Tetrahedron : public Figure {
public:
	Tetrahedron();
};

class Octahedron : public Figure {
public:
	Octahedron();
};

class Icosahedron : public Figure {
public:
	Icosahedron();
	std::vector<Vector3D> GenerateDodecahedronPoints();
};

class Dodecahedron : public Figure {
public:
	Dodecahedron();

};

class Sphere : public Figure {
public:
	Sphere();
	void Generate(Icosahedron& ico);
	void Create(int n);
};

class Cone : public Figure {
public:
	Cone();
	void Create(int n, int height);
};

class Cylinder : public Figure {
public:
	Cylinder();
	void Create(int n, int height);
};

class Torus : public Figure {
public:
	Torus();
	void Create(int n, int m, double r, double R);
};

}/* namespace Figures */

#endif /* FIGURES_HH_ */
