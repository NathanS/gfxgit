/*
 * Face.hh
 *
 *  Created on: Aug 9, 2014
 *      Author: Nathan Steurs
 */

#ifndef FACE_HH_
#define FACE_HH_

#include <vector>

class Face {
public:
	Face();
	virtual ~Face();
	std::vector<int> point_indexes;
};

#endif /* FACE_HH_ */
