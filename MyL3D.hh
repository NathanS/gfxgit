/*
 * MyL3D.h
 *
 *  Created on: Aug 17, 2014
 *      Author: Nathan Steurs
 */

#ifndef MYL3D_H_
#define MYL3D_H_

#include "lparser.h"
#include <istream>
#include <stack>
#include "ini_configuration.hh"
#include "EasyImage.h"
#include "Figure.hh"
#include "line.h"
#include <cmath>

class Position3D {
public:
	Position3D();
	Position3D(double x, double y, double z, Vector3D _H, Vector3D _L, Vector3D _U);
	virtual ~Position3D();

	double getX() const {
		return x;
	}

	double getY() const {
		return y;
	}

	double getZ() const {
		return z;
	}

	const Vector3D& getH() const {
		return H;
	}

	const Vector3D& getL() const {
		return L;
	}

	const Vector3D& getU() const {
		return U;
	}

private:
	double x;
	double y;
	double z;
	Vector3D H;
	Vector3D L;
	Vector3D U;
};

class MyL3D : public LParser::LSystem3D, public Figure {
public:
	MyL3D(std::istream &in);
	MyL3D();
	void Create();
	bool Exists(char& c);
	void Iterate(std::string s, int i);
	void GenPoints();
	void GenLines();
	double getAngle();
	void To2D();
	void SetCenter(std::vector<double>);
	void SetColor(img::Color);
	void AddLine(double x1, double y1, double z1, double x2, double y2, double z2);
	double DegToRad(double angle);
	//void GetInfoConf(ini::Configuration &config, int n);
	virtual ~MyL3D();

	const std::vector<line>& getLsyslines() const
	{
		return lsyslines;
	}

	void setLsyslines(const std::vector<line>& lsyslines)
	{
		this->lsyslines = lsyslines;
	}

private:
	double my_x;
	double my_y;
	double my_z;
	std::stack<Position3D> Brackets;
	std::vector<line> lsyslines;
	Vector3D H;
	Vector3D L;
	Vector3D U;
};


#endif /* MYL3D_H_ */
