/*
 * Figure.hh
 *
 *  Created on: Aug 9, 2014
 *      Author: Nathan Steurs
 */

#ifndef FIGURE_HH_
#define FIGURE_HH_

#include "Face.hh"
#include <vector>
#include "vector.hh"
#include "EasyImage.h"
#include <list>
#include "Point.hh"
#include "line.h"
#include "ini_configuration.hh"
#include "DrawHelpers.hh"

class Figure {
public:
	Figure();
	Figure& operator=(Figure& f);
	void RotateX(double angle);
	void RotateY(double angle);
	void RotateZ(double angle);
	void Scale(double scale);
	void Translate(Vector3D vector);
	void Translate();
	void TransformEye(Vector3D eye);
	void GetInfoConf(const ini::Configuration& conf, int figurenumber);
	void ToRadAngles();
	void GenerateLines();
	void Project();
	void AddLine(line l);
	void AddLine(double x1, double y1,double z1,double x2, double y2, double z2, img::Color clr);
	std::vector<line>& getLines();
	std::vector<Vector3D>& getPoints();
	virtual ~Figure();

	const img::Color& getColor() const {
		return color;
	}

	const std::vector<Face>& getFaces() const {
		return faces;
	}

	double getScale() const {
		return scale;
	}

	const std::string& getType() const {
		return type;
	}

	double getXangle() const {
		return xangle;
	}

	double getYangle() const {
		return yangle;
	}

	double getZangle() const {
		return zangle;
	}

	void setType(const std::string& type) {
		this->type = type;
	}

	void setFaces(const std::vector<Face>& faces) {
		this->oldfaces = this->faces;
		this->faces = faces;
	}

	void setPoints(const std::vector<Vector3D>& points) {
		this->points = points;
	}

	void setLines(const std::vector<line>& lines) {
		this->lines = lines;
	}

	const std::vector<Vector3D>& get_initialpoints() const
	{
		return initialpoints;
	}

	void set_initialpoints(const std::vector<Vector3D>& initialpoints)
	{
		this->initialpoints = initialpoints;
	}

protected:
	std::vector<Vector3D> points;
	std::vector<Vector3D> initialpoints;
	std::vector<Face> faces;
	img::Color color;
	double xangle;
	double yangle;
	double zangle;
	double scale;
	std::vector<double> center;
	std::vector<line> lines; //lines to draw on 2D
	std::string type;
	std::vector<Face> oldfaces;
private:

};

#endif /* FIGURE_HH_ */
