/*
 * DrawHelpers.hh
 *
 *  Created on: Aug 13, 2014
 *      Author: Nathan Steurs
 */

#ifndef DRAWHELPERS_HH_
#define DRAWHELPERS_HH_

#include "line.h"
#include "EasyImage.h"
#include "Figure.hh"
#include <vector>

namespace drawing {

void CalculateParam(long long int size, long double& dx, long double& dy, long double& imagex, long double& imagey,
        long double& d, std::vector<line> lines);
double Xmax(std::vector<line> lines);
double Ymax(std::vector<line> lines);
double Xmin(std::vector<line> lines);
double Ymin(std::vector<line> lines);
void Scale(long double& dx, long double& dy, long double& d, std::vector<line>& lines);
void Draw(img::EasyImage& img, std::vector<line> lines);
void PrettyPrint(std::vector<line> l);
void PrettyPrint(std::vector<Vector3D> p);
void PrettyPrint(std::vector<double> p);
img::Color GetColour(std::vector<double> p);
void Triangulate(Face const& face, std::vector<Face>& faces);

}
#endif /* DRAWHELPERS_HH_ */
