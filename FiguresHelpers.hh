/*
 * FiguresHelpers.hh
 *
 *  Created on: Aug 16, 2014
 *      Author: Nathan Steurs
 */

#ifndef FIGURESHELPERS_HH_
#define FIGURESHELPERS_HH_

#include "Figures.hh"
#include "MyL3D.hh"

namespace FigureHelp {

void AssignCorrectType(Figure& f, const ini::Configuration& config, int fignr);

}



#endif /* FIGURESHELPERS_HH_ */
