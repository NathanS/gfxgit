/*
 * line.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: Nathan Steurs
 */

#include "line.h"

line::line() {
	// TODO Auto-generated constructor stub
	x1 = 0;
	x2 = 0;
	y1 = 0;
	y2 = 0;
	z1 = 0;
	z2 = 0;
}

line::line(double a, double b, double c, double d){
	x1 = a;
	x2 = b;
	y1 = c;
	y2 = d;
	z1 = 0;
	z2 = 0;
}
line::line(double a, double b, double c, double d, double e, double f, img::Color clr){
	x1 = a;
	x2 = b;
	y1 = c;
	y2 = d;
	z1 = e;
	z2 = f;
	color = clr;
}

line::line(double a, double b, double c, double d, double e, double f){
	x1 = a;
	x2 = b;
	y1 = c;
	y2 = d;
	z1 = e;
	z2 = f;
}

line::line(double a, double b, double c, double d, img::Color clr){
	x1 = a;
	x2 = b;
	y1 = c;
	y2 = d;
	z1 = 0;
	z2 = 0;
	color = clr;
}
void line::ShowFullObject(){
	std::cout << "Printing out the entire Line Object:" << std::endl;
	std::cout << "[X0 = " << this->x1 << "][Y0 = " << this->y1 << "][Z0 = " << this->z1 << "]" << std::endl;
	std::cout << "[X1 = " << this->x2 << "][Y1 = " << this->y2 << "][Z1 = " << this->z2 << "]" << std::endl;
}
img::Color& line::getColor(){
	return color;
}

void line::Show(){
	std::cout << "[[[[";
	std::cout<< getX1() << "|" << getY1() <<  std::endl <<  x2 << "|" << y2 << std::endl;
	std::cout << "]]]]";
}
line::~line() {
	// TODO Auto-generated destructor stub
}

