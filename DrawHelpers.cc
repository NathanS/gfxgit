/*
 * DrawHelpers.cc
 *
 *  Created on: Aug 13, 2014
 *      Author: Nathan Steurs
 */
#include "DrawHelpers.hh"
#include <vector>
#include <algorithm>
#include <cmath>

using namespace drawing;

double drawing::Xmin(std::vector<line> lines)
{
	double temp = 200000;
	for (auto line : lines) {
		if (line.getX1() < temp) {
			temp = line.getX1();
		}
		if (line.getX2() < temp) {
			temp = line.getX2();
		}
	}
	return temp;
}
double drawing::Ymin(std::vector<line> lines)
{
	double temp = 200000;
	for (auto line : lines) {
		if (line.getY1() < temp) {
			temp = line.getY1();
		}
		if (line.getY2() < temp) {
			temp = line.getY2();
		}
	}
	return temp;
}
double drawing::Xmax(std::vector<line> lines)
{
	double temp = -200000;
	for (auto line : lines) {
		if (line.getX1() > temp) {
			temp = line.getX1();
		}
		if (line.getX2() > temp) {
			temp = line.getX2();
		}
	}
	return temp;
}
double drawing::Ymax(std::vector<line> lines)
{
	double temp = -200000;
	for (auto line : lines) {
		if (line.getY1() > temp) {
			temp = line.getY1();
		}
		if (line.getY2() > temp) {
			temp = line.getY2();
		}
	}
	return temp;
}

void drawing::CalculateParam(long long int size, long double& dx, long double& dy, long double& imageX,
        long double& imageY, long double& d, std::vector<line> lines)
{

	long double xRange = (Xmax(lines) - Xmin(lines));
	long double yRange = (Ymax(lines) - Ymin(lines));
	imageX = size * (xRange / (std::max(xRange, yRange)));
	imageY = size * (yRange / (std::max(xRange, yRange)));
	d = 0.95 * (imageX / xRange);
	long double DcX = ((d * (Xmin(lines) + Xmax(lines))) / 2.0);
	long double DcY = ((d * (Ymin(lines) + Ymax(lines))) / 2.0);
	dx = (imageX / 2.0) - DcX;
	dy = (imageY / 2.0) - DcY;
}

void drawing::PrettyPrint(std::vector<double> points)
{
	std::cout << "All Points:" << std::endl;
	for (auto point : points) {
		std::cout << point << std::endl;
	}

}

void drawing::PrettyPrint(std::vector<Vector3D> points)
{
	std::cout << "All Points:" << std::endl;
	for (auto point : points) {
		std::cout << "[" << point.x << "\t" << point.y << "\t" << point.z << "]" << std::endl;
	}
}

void drawing::PrettyPrint(std::vector<line> lines)
{
	std::cout << "PrettyPrint was used on vector of size:" << lines.size() << std::endl;
	for (auto line : lines) {
		std::cout << "[" << line.getX1() << "\t" << line.getY1() << "]";
		std::cout << "[" << line.getX2() << "\t" << line.getY2() << "]" << std::endl;
	}
}

void drawing::Scale(long double& dx, long double& dy, long double& d, std::vector<line>& lines)
{
	for (auto &line : lines) {
		line.setX1((line.getX1() * d) + dx);
		line.setX2((line.getX2() * d) + dx);
		line.setY1((line.getY1() * d) + dy);
		line.setY2((line.getY2() * d) + dy);
	}
}
void drawing::Draw(img::EasyImage& img, std::vector<line> lines)
{
	//std::cout << img.get_width() << "AND" << img.get_height() << std::endl;
	for (auto line : lines) {
		img.draw_line(round(line.getX1()), round(line.getY1()), round(line.getX2()), round(line.getY2()),
		        line.getColor());
	}
}

img::Color drawing::GetColour(std::vector<double> p)
{
	img::Color c;
	c.red = round(p[0] * 255);
	c.green = round(p[1] * 255);
	c.blue = round(p[2] * 255);
	return c;

}

/*
 * Convert a simple face into a new faces (triangles)
 */
void drawing::Triangulate(Face const& face, std::vector<Face>& faces)
{
	auto i = 0;
	std::vector<Face> tmpfaces;
	for (i = 1; i < face.point_indexes.size() - 1; i++) {
		Face tmpface;
		tmpface.point_indexes.push_back(face.point_indexes[0]);
		tmpface.point_indexes.push_back(face.point_indexes[i]);
		tmpface.point_indexes.push_back(face.point_indexes[i + 1]);
		tmpfaces.push_back(tmpface);
	}
	//std::cout << "Faces:" << faces.size() << std::endl;
	//std::cout << "Tmpfaces" << tmpfaces.size() << std::endl;
	faces = tmpfaces;
}
