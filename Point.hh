/*
 * Point.hh
 *
 *  Created on: Aug 11, 2014
 *      Author: Nathan Steurs
 */

#ifndef POINT_HH_
#define POINT_HH_

class Point {
public:
	Point();
	Point(double a, double b, double c);
	void setZ(double newz);
	virtual ~Point();
	double x;
	double y;

private:
	double z;
};

#endif /* POINT_HH_ */
