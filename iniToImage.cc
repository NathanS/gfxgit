/*
 * iniToImage.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: Nathan Steurs
 */

#include "iniToImage.hh"
#include <string>
#include <fstream>
#include <cmath>

using namespace ini2image;

iniToImage::iniToImage()
{
}

iniToImage::~iniToImage()
{
	// TODO Auto-generated destructor stub
}

img::EasyImage iniToImage::processIniL3D(const ini::Configuration &config)
{
	img::Color color;
	int size = config["General"]["size"].as_int_or_die();
	std::vector<double> colorBG = config["General"]["backgroundcolor"].as_double_tuple_or_die();
	std::string filename = config["3DLSystem"]["inputfile"].as_string_or_die();
	std::vector<double> colorPrim = config["3DLSystem"]["color"].as_double_tuple_or_die();
	int bgc[3];
	int primc[3];
	for (int i = 0; i < colorBG.size() - 1; i++) {
		bgc[i] = round(colorBG[i] * 255);
		primc[i] = round(colorPrim[i] * 255);
	}
	color.red = primc[0];
	color.green = primc[1];
	color.blue = primc[2];
	img::Color bgColor;
	bgColor.red = bgc[0];
	bgColor.green = bgc[1];
	bgColor.blue = bgc[2];
	const char* test = filename.c_str();
	std::ifstream in(test);
	MyL3D l(in);
	l.GetInfoConf(config, 0);
	l.Create();
	l.To2D();
}

img::EasyImage iniToImage::processIniL2D(const ini::Configuration &config)
{
	img::Color color;
	int size = config["General"]["size"].as_int_or_die();
	std::vector<double> colorBG = config["General"]["backgroundcolor"].as_double_tuple_or_die();
	std::string filename = config["2DLSystem"]["inputfile"].as_string_or_die();
	std::vector<double> colorPrim = config["2DLSystem"]["color"].as_double_tuple_or_die();
	int bgc[3];
	int primc[3];
	for (int i = 0; i < colorBG.size() - 1; i++) {
		bgc[i] = round(colorBG[i] * 255);
		primc[i] = round(colorPrim[i] * 255);
	}
	color.red = primc[0];
	color.green = primc[1];
	color.blue = primc[2];
	img::Color bgColor;
	bgColor.red = bgc[0];
	bgColor.green = bgc[1];
	bgColor.blue = bgc[2];
	const char* test = filename.c_str();
	std::ifstream in(test);
	MyL2D l2d(in);
	l2d.Create();
	return l2d.ScaleSystem(size, color, bgColor);
}

img::EasyImage iniToImage::processIniWireframe(const ini::Configuration& config)
{
	auto size = config["General"]["size"].as_int_or_die();
	std::vector<double> colorBG = config["General"]["backgroundcolor"].as_double_tuple_or_die();
	long long int nrFigures = config["General"]["nrFigures"].as_int_or_die();
	std::vector<double> eye = config["General"]["eye"].as_double_tuple_or_die();
	Vector3D eyepoint = Vector3D::point(eye[0], eye[1], eye[2]);
	std::vector<Figure> figures;
	std::vector<line> Linestodraw;
	for (int i = 0; i < nrFigures; i++) {
		/*
		 * Need a separate loop for L3D systems
		 * TODO: UNIFY
		 */
		if (config["Figure" + std::to_string(i)]["type"].as_string_or_die() == "3DLSystem") {
			std::string filename = config["Figure" + std::to_string(i)]["inputfile"].as_string_or_die();
			int scale = config["Figure" + std::to_string(i)]["scale"].as_int_or_die();
			double rx = config["Figure" + std::to_string(i)]["rotateX"].as_int_or_die();
			double ry = config["Figure" + std::to_string(i)]["rotateY"].as_int_or_die();
			double rz = config["Figure" + std::to_string(i)]["rotateZ"].as_int_or_die();
			std::vector<double> _center =
			        config["Figure" + std::to_string(i)]["center"].as_double_tuple_or_die();
			std::vector<double> color =
			        config["Figure" + std::to_string(i)]["color"].as_double_tuple_or_die();
			const char* test = filename.c_str();
			std::ifstream in(test);
			MyL3D l3d(in);
			l3d.SetCenter(_center);
			l3d.SetColor(drawing::GetColour(color));
			l3d.Create(); //put everything in lsyslines
			l3d.GenPoints(); //put every point in points from lsyslines
			l3d.Scale(scale);
			l3d.RotateX(rx);
			l3d.RotateY(ry);
			l3d.RotateZ(rz);
			l3d.Translate();
			l3d.TransformEye(eyepoint);
			//drawing::PrettyPrint(l3d.getPoints());
			//HG
			l3d.Project();
			l3d.GenLines(); //Create the lines to draw
			for (auto lijn : l3d.getLines()) {
				//	lijn.Show();
				Linestodraw.push_back(lijn);
			}
			//figures.push_back(l3d);
		} else if (config["General"]["type"].as_string_or_die() == "ZBuffering") {

			Figure newfig;
			newfig.GetInfoConf(config, i);
			FigureHelp::AssignCorrectType(newfig, config, i);

			/*
			 * For all our original faces, divide them into triangles and
			 * save those as the new faces to Zbuffer.
			 */
			//newfig.SaveFaces();//save old faces
			std::vector<Face> figurefaces;
			for (auto z = 0; z < newfig.getFaces().size(); z++) {
				std::vector<Face> newfaces;
				drawing::Triangulate(newfig.getFaces()[z], newfaces);
				figurefaces.insert(figurefaces.end(), newfaces.begin(), newfaces.end());
			}
			//std::cout << "Number of faces after Triangulation:" << figurefaces.size() << std::endl;
			//std::cout << "Number of Faces before Triangulation:" << newfig.getFaces().size() << std::endl;
			newfig.setFaces(figurefaces); //Also saves old faces in oldfaces
			/*
			 * Continue configuring the points for the image.
			 */
			newfig.Scale(newfig.getScale());
			newfig.RotateX(newfig.getXangle());
			newfig.RotateY(newfig.getYangle());
			newfig.RotateZ(newfig.getZangle());
			newfig.Translate();
			newfig.TransformEye(eyepoint);

			//newfig.set_initialpoints(newfig.getPoints());
			newfig.set_initialpoints(newfig.getPoints());
			/*
			 * Save the points because we need them for the
			 * Zbuffer with triangles routine.
			 */
			newfig.Project();
			figures.push_back(newfig);
		} else {
			/*
			 * Standard figure procedure
			 */
			Figure newfig;
			newfig.GetInfoConf(config, i);
			FigureHelp::AssignCorrectType(newfig, config, i);
			newfig.Scale(newfig.getScale());
			newfig.RotateX(newfig.getXangle());
			newfig.RotateY(newfig.getYangle());
			newfig.RotateZ(newfig.getZangle());
			newfig.Translate();
			newfig.TransformEye(eyepoint);
			newfig.Project();
			figures.push_back(newfig);
		}
	}
	for (auto &figure : figures) {
		figure.GenerateLines();
		for (auto lijn : figure.getLines()) {
			Linestodraw.push_back(lijn);
		}
	}

	long double dx = 0.0;
	long double dy = 0.0;
	long double imageX = 0.0;
	long double imageY = 0.0;
	long double d = 0.0;
	drawing::CalculateParam(size, dx, dy, imageX, imageY, d, Linestodraw);
	drawing::Scale(dx, dy, d, Linestodraw);
	//std::cout << "ImageX:" << imageX << std::endl << "ImageY:" << imageY << std::endl;
	//std::cout << "Dx:" << dx << "|" << "Dy:" << dy << "|" << "d:"<< d << std::endl;
	img::EasyImage img(round(imageX + 0.5), round(imageY + 0.5), drawing::GetColour(colorBG));
	if (config["General"]["type"].as_string_or_die() == "ZBufferedWireframe") {
		//Create and use Zbuffer
		std::cout << "[Creating a Zbuffered Image]" << std::endl;
		Zbuffer z(round(imageX + 0.5), round(imageY + 0.5));
		z.Draw(img, Linestodraw);
	} else if (config["General"]["type"].as_string_or_die() == "ZBuffering") {
		std::cout << "[Creating a Triangulated Zbuffered Image]" << std::endl;
		Zbuffer z(round(imageX), round(imageY));
		//std::cout << imageX << "   " << imageY << std::endl;
		//std::cout << d << "#" << dx << "#" << dy << std::endl;
		//z.Draw(img, Linestodraw);
		z.T_draw_zbuf(figures, img, d, dx, dy);
		//std::cout << img.get_height();
		//std::cout << img.get_width();
		//std::cout << std::endl;
		//std::cout << "Graceful exit" << std::endl;

	} else {
		//Normal Line Drawing, no z Buffer.
		drawing::Draw(img, Linestodraw);
	}

	return img;
}
